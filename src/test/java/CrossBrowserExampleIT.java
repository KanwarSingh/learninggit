import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

/* 
 * Requirements: 
 * 
 * 1. Access the site:  http://edteam.prodigygame.org/qa/
 *    username and password to access this site is edteam/BeanSproutMints3
 * 2. Verify the question total for skill number = 3, ie. expected question total = 45
 * 3. Default webdriver is firefox
 * 3. If you have enough time, please run the test with chrome and safari as well
 * 
 * 
 * */


public class CrossBrowserExampleIT {
	private WebDriver driver;


	@BeforeMethod
	public void setUp() {
		System.setProperty("webdriver.gecko.driver", ",/Driver\\geckodriver.exe");
		driver = new FirefoxDriver();
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
		driver.get("http://www.google.com");
	}

	@AfterMethod
	public void tearDown() {
		driver.close();
	}
}
